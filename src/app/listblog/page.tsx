import React from "react";
import SectionLargeSlider from "@/app/(home)/SectionLargeSlider";
import BackgroundSection from "@/components/BackgroundSection/BackgroundSection";
import Heading1 from "@/components/A_ListBlog/Heading1";
import {
  DEMO_POSTS,
  DEMO_POSTS_AUDIO,
  DEMO_POSTS_GALLERY,
  DEMO_POSTS_VIDEO,
} from "@/data/posts";
import { DEMO_CATEGORIES } from "@/data/taxonomies";
import SectionBlog from "@/components/A_ListBlog/SectionBlog";
import SectionBlog1 from "@/components/A_ListBlog/SectionBlog1";
import SectionBlog2 from "@/components/A_ListBlog/SectionBlog2";
import SectionSliderPosts from "@/components/Sections/SectionSliderPosts";
import SectionMagazine1 from "@/components/Sections/SectionMagazine1";
import SectionAds from "@/components/Sections/SectionAds";
import SectionMagazine7 from "@/components/Sections/SectionMagazine7";
import SectionGridPosts from "@/components/Sections/SectionGridPosts";
import SectionMagazine8 from "@/components/Sections/SectionMagazine8";
import SectionMagazine9 from "@/components/A_ListBlog/SectionMagazine9";
import SectionGridAuthorBox from "@/components/SectionGridAuthorBox/SectionGridAuthorBox";
import SectionBecomeAnAuthor from "@/components/SectionBecomeAnAuthor/SectionBecomeAnAuthor";
import SectionSubscribe2 from "@/components/SectionSubscribe2/SectionSubscribe2";
import SectionVideos from "@/components/Sections/SectionVideos";
import SectionLatestPosts from "@/components/Sections/SectionLatestPosts";
import SectionMagazine2 from "@/components/A_ListBlog/SectionMagazine2";

//
const MAGAZINE1_POSTS = DEMO_POSTS.filter((_, i) => i >= 8 && i < 16);
const MAGAZINE2_POSTS = DEMO_POSTS.filter((_, i) => i >= 0 && i < 7);
//

const PageHome = ({}) => {
  return (
    <div className="nc-PageHome relative">
      <div className="container relative">

        <div className="relative py-16">

          <SectionMagazine2
            heading="Newest authors"
            subHeading="Say hello to future creator potentials"
            posts={MAGAZINE2_POSTS}
          />
        </div>

        <SectionMagazine9 />

            <div className="relative">
              <div className="">
                <SectionBlog posts={MAGAZINE1_POSTS} />
              </div>
            </div>


            <div className="relative">
              <div className="">
                <SectionBlog1 posts={MAGAZINE1_POSTS} />
              </div>
            </div>

            <div className="relative">
              <div className="">
                <SectionBlog2 posts={MAGAZINE1_POSTS} />
              </div>
            </div>
   
      </div>
    </div>
  );
};

export default PageHome;
