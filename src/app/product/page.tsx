"use client";
import React, { useEffect, useState } from "react";
import { DEMO_POSTS_NEWS } from "@/data/posts";
import LeftPro from "@/components/A_detailsPro/LeftPro";
import RightPro from "@/components/A_detailsPro/RightPro";
import Header from "@/components/A_detailsPro/Header";
import { TaxonomyType } from "@/data/types";
import { Route } from "@/routers/types";

const MAGAZINE1_POSTS = DEMO_POSTS_NEWS.filter((_, i) => i >= 8 && i < 16);
const MAGAZINE2_POSTS = DEMO_POSTS_NEWS.filter((_, i) => i >= 0 && i < 7);

const TRAVEL_SUBCATS: TaxonomyType[] = [
  {
    id: 1,
    name: "New York",
    href: "/archive/the-demo-archive-slug" as Route,
    thumbnail:
      "https://images.pexels.com/photos/2179602/pexels-photo-2179602.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
    count: 132,
    color: "pink",
    taxonomy: "category",
  },
  // ... (rest of TRAVEL_SUBCATS)
];

const Homepage: React.FC = () => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
        <div className="relative">
      <Header className="" />
      <div className={`flex ${isMobile ? 'flex-col' : 'flex-row'} lg:flex-row container`}>
        <div className={isMobile ? "w-full" : "lg:w-4/12"}>
          <div className={`mt-24 lg:mt-24 lg:ml-12 ${isMobile ? 'lg:mt-0 -ml-4' : 'lg:fixed z-10'}`}>
            <LeftPro className="" />
          </div>
        </div>

        <div className={isMobile ? "w-full mt-8" : " w-full lg:w-9/12 mt-24 lg:mt-24"}>
          <div className="relative overflow-hidden loading=lazy">
            <RightPro />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Homepage;