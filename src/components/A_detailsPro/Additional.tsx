'use client'
import React, { FC, useState, useEffect } from "react";
import Card21 from "@/components/Card21/Card21";
import WidgetHeading1 from "@/components/A_detailsPro/WidgetHeading1";
import { TaxonomyType } from "@/data/types";
import { DEMO_TAGS } from "@/data/taxonomies";

const tagsDemo = DEMO_TAGS.filter((_, i) => i < 5);

export interface WidgetTagsProps {
  className?: string;
  tags?: TaxonomyType[];
}

const WidgetTags: FC<WidgetTagsProps> = ({
  className = "bg-gray-100 dark:bg-neutral-800",
  tags = tagsDemo,
}) => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);


  return (
    <>
      <WidgetHeading1 title=" Additional resources" />
      <div className={`nc-WidgetTags rounded-xl overflow-hidden ${className}`}>
        {!isMobile && (
           <div className="flex flex-wrap p-4 xl:p-5">
           {tags.map((tag) => (
             <Card21 className="mr-9 mb-5 " key={tag.id} tag={tag} />
           ))}
         </div>
        )}

        {isMobile && (
        <div className="container flex flex-wrap p-4 xl:p-5">
          
          {tags.map((tag) => (
            <Card21
              className="mb-5 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5"
              key={tag.id}
              tag={tag}
            />
          ))}
        </div>
        )}
      </div>
    </>
  );
};

export default WidgetTags;