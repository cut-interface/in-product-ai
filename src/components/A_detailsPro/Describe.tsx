import React, { FC, useState, useEffect } from "react";
import Heading from "@/components/A_detailsPro/Heading";

export interface SectionAdsProps {
  className?: string;
  index?: string;
}

interface Paragraph {
  title: string;
  content: string;
}

const paragraphs: Paragraph[] = [
  {
    title: "Edit and trim with total precision",
    content:
      "Combine various tasks like Batch Rename, Image Resize, Format Conversion, and Metadata template to create your own Workflows for repeat use. Use the Workflow Builder workspace in Bridge to stitch multiple tasks and run it by just dragging and dropping your creative assets on workflow presets. Create PDF contact sheets using the Output workspace and publish multiple assets directly to your Adobe Stock or Adobe Portfolio easily from Bridge using the Publish panel.",
  },
  {
    title: "Seamless photos management with powerful preview & selection workflows",
    content: "Industry-standard tools help you craft the perfect story.",
  },
  {
    title: "Do more with your video assets in Bridge",
    content: "Industry-standard tools help you craft the perfect story.",
  },
  {
    title: "Visually Manage your Substance 3D materials in Bridge",
    content: "Industry-standard tools help you craft the perfect story.",
  },
];

const Describe: FC<SectionAdsProps> = () => {
  const [activeIndex, setActiveIndex] = useState<number | null>(null);

  const handleHeaderClick = (index: number) => {
    setActiveIndex((prevIndex) => (prevIndex === index ? null : index));
  };

  useEffect(() => {
    // Mở rộng đoạn văn đầu tiên khi trang được tạo
    setActiveIndex(0);
  }, []); // Chạy chỉ một lần sau khi trang được tạo
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <>
      <Heading />

      <div className={`relative flex flex-col group bg-white dark:bg-neutral-900 rounded-sm w-full h-auto mt-8 ${isMobile ? "container" : ""}`}>
        <div className="items-center mt-3">
          {paragraphs.map((paragraph, index) => ( 
            <div className="m-4" key={index}>
              <h2
                className={`text-sm sm:text-sm text-gray-700 font-medium sm:font-semibold cursor-pointer hover:font-bold`}
                onClick={() => handleHeaderClick(index)}
              >
                {paragraph.title}
              </h2>
              {activeIndex === index && (
                <p className="text-xs sm:text-xs text-gray-500 font-medium sm:font-semibold mt-2">{paragraph.content}</p>
              )}
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default Describe;