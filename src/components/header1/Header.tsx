"use client"
import React, { FC,useState, useEffect } from "react";
import MainNav2 from "./Navtop";
import Navleft1 from "./Navleft1";
export interface HeaderProps {}

const Header: FC<HeaderProps> = () => {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className="nc-Header sticky top-0 w-full z-40">
      <MainNav2 />
      
        {!isMobile && <Navleft1 />}
        </div>
    
  );
};

export default Header;
