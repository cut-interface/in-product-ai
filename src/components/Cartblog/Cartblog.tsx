"use client";

import React, { FC, useState } from "react";
import PostCardSaveAction from "@/components/PostCardSaveAction/PostCardSaveAction";
import { PostDataType } from "@/data/types";
import CategoryBadgeList from "@/components/CategoryBadgeList/CategoryBadgeList";
import PostCardLikeAndComment from "@/components/PostCardLikeAndComment/PostCardLikeAndComment";
import PostCardMeta from "@/components/PostCardMeta/PostCardMeta";
import PostFeaturedMedia from "@/components/Cartblog/PostFeaturedMedia";
import Link from "next/link";

export interface Card11Props {
  className?: string;
  post: PostDataType;
  ratio?: string;
  hiddenAuthor?: boolean;
}

const Cartblog: FC<Card11Props> = ({
  className = "h-full",
  post,
  hiddenAuthor = false,
  ratio = "aspect-w-4 aspect-h-3",
}) => {
  const { title, href, categories, date } = post;



  return (
    <div
      className={`nc-Card11 relative flex flex-col group rounded-sm border-2 border-solid hover:border-gray-300  overflow-hidden bg-white dark:bg-neutral-900 ${className}`}

      //
    >
      <div
        className={`block flex-shrink-0 relative w-full rounded-t-sm overflow-hidden z-10 ${ratio}`}
      >
        <div>
          <PostFeaturedMedia post={post}  />
        </div>
      </div>
      <Link href={href} className="absolute inset-0"></Link>
      <span className="absolute top-3 inset-x-3 z-10">
        <CategoryBadgeList categories={categories} />
      </span>

      <div className="p-4 flex flex-col space-y-3">
        <h3 className="nc-card-title block text-base font-semibold text-neutral-900 dark:text-neutral-100">
          <span className="line-clamp-2" title={title}>
            {title}
          </span>
        </h3>
        <div className="flex items-end justify-between mt-auto">
          <PostCardSaveAction className="relative" />
        </div>
      </div>
    </div>
  );
};

export default Cartblog;
