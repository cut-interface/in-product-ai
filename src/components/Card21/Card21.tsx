import { TaxonomyType } from "@/data/types";
import Link from "next/link";
import Image from "next/image";
import React, { FC } from "react";

export interface TagProps {
  className?: string;
  tag: TaxonomyType;
  hideCount?: boolean;
}

const Card21: FC<TagProps> = ({ className = "", tag, hideCount = false }) => {
  return (

      

    <Link
      className={`nc-Tag w-56 flex bg-white hover:border-gray-400 text-sm border-2 border-solid text-neutral-600 dark:text-neutral-300 py-2 px-3 rounded-sm md:py-2.5 md:px-4 dark:bg-neutral-900 ${className}`}
      href={tag.href}
    >
<div>
<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-badge-hd mt-2" viewBox="0 0 16 16">
  <path d={tag.svg} />
  <path d={tag.svg1} />
</svg>
</div>
<h2 className="m-3 text-sm">{`${tag.name}`}</h2>

    </Link>

  );
};

export default Card21;
