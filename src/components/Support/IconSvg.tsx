import React from "react";

const IconSvg = () => {
  return (
    <svg
      width="50"
      height="50"
      viewBox="0 0 24 24"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
    >
     <path d="M11 9.5 A1.5 1.5 0 0 1 9.5 11 A1.5 1.5 0 0 1 8 9.5 A1.5 1.5 0 0 1 11 9.5 z" />
      <path d="M16 9.5 A1.5 1.5 0 0 1 14.5 11 A1.5 1.5 0 0 1 13 9.5 A1.5 1.5 0 0 1 16 9.5 z" />
      <path d="M12 2C6.486 2 2 5.589 2 10c0 2.908 1.897 5.515 5 6.934V22l5.34-4.004C17.697 17.852 22 14.32 22 10c0-4.411-4.486-8-10-8zm0 14h-.333L9 18v-2.417l-.641-.247C5.671 14.301 4 12.256 4 10c0-3.309 3.589-6 8-6s8 2.691 8 6-3.589 6-8 6z" />
    </svg>
  );
};

export default IconSvg;
