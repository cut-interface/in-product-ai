import React from "react";
import Logo from "@/components/Logo/Logo";
import SocialsList1 from "@/components/SocialsList1/SocialsList1";
import { CustomLink } from "@/data/types";
import HeadingCate from "@/components/Heading/HeadingSearch"
export interface WidgetMenu {
  id: string;
  title: string;
  menus: CustomLink[];
}

const widgetMenus: WidgetMenu[] = [


  {
    id: "1",
    title: "Resources",
    menus: [
      { href: "/", label: "Best practices" },
      { href: "/", label: "Support" },
      { href: "/", label: "Developers" },
      { href: "/", label: "Learn design" },
      { href: "/", label: "What's new" },
    ],
  },
  {
    id: "2",
    title: "Community",
    menus: [
      { href: "/", label: "Discussion Forums" },
      { href: "/", label: "Code of Conduct" },
      { href: "/", label: "Community Resources" },
      { href: "/", label: "Contributing" },
      { href: "/", label: "Concurrent Mode" },
    ],
  },
  {
    id: "2",
    title: "Community",
    menus: [
      { href: "/", label: "Discussion Forums" },
      { href: "/", label: "Code of Conduct" },
      { href: "/", label: "Community Resources" },
      { href: "/", label: "Contributing" },
      { href: "/", label: "Concurrent Mode" },
    ],
  },
  {
    id: "2",
    title: "Community",
    menus: [
      { href: "/", label: "Discussion Forums" },
      { href: "/", label: "Code of Conduct" },
      { href: "/", label: "Community Resources" },
      { href: "/", label: "Contributing" },
      { href: "/", label: "Concurrent Mode" },
    ],
  },
  {
    id: "2",
    title: "Community",
    menus: [
      { href: "/", label: "Discussion Forums" },
      { href: "/", label: "Code of Conduct" },
      { href: "/", label: "Community Resources" },
      { href: "/", label: "Contributing" },
      { href: "/", label: "Concurrent Mode" },
    ],
  },
  {
    id: "2",
    title: "Community",
    menus: [
      { href: "/", label: "Discussion Forums" },
      { href: "/", label: "Code of Conduct" },
      { href: "/", label: "Community Resources" },
      { href: "/", label: "Contributing" },
      { href: "/", label: "Concurrent Mode" },
    ],
  },
];

const Footer: React.FC = () => {
  const renderWidgetMenuItem = (menu: WidgetMenu, index: number) => {
    return (
      <div key={index} className="text-sm">
        <h2 className="font-semibold text-neutral-700 dark:text-neutral-200">
          {menu.title}
        </h2>
        <ul className="mt-5 space-y-4">
          {menu.menus.map((item, index) => (
            <li key={index}>
              <a
                key={index}
                className="text-neutral-6000 dark:text-neutral-300 hover:text-black dark:hover:text-white"
                href={item.href}
              >
                {item.label}
              </a>
            </li>
          ))}
        </ul>
      </div>
    );
  };

  return (
    
    <div className="nc-Footer relative py-16 lg:py-28 border-t border-neutral-200 dark:border-neutral-700">
        <HeadingCate/>
      <div className="grid grid-cols-2 gap-y-10 gap-x-5 sm:gap-x-8 md:grid-cols-4 lg:grid-cols-6 lg:gap-x-8 ">
        {widgetMenus.map(renderWidgetMenuItem)}
      </div>
    </div>
  );
};

export default Footer;
